use std::{
    pin::Pin,
    task::{Context, Poll},
};

use bytes::Bytes;
use futures::Stream;
use smoltcp::{iface::SocketHandle, wire::IpEndpoint};
use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};

use crate::IFace;

pub struct TcpSocket {
    handle: SocketHandle,
    iface: IFace,
}

pub struct UdpSocket {
    handle: SocketHandle,
    iface: IFace,
}

impl UdpSocket {
    pub fn new(iface: IFace, s: smoltcp::socket::UdpSocket<'static>) -> Self {
        let handle = iface.lock().unwrap().add_socket(s);
        Self { handle, iface }
    }

    fn with<R>(&mut self, f: impl FnOnce(&mut smoltcp::socket::UdpSocket) -> R) -> R {
        f(self
            .iface
            .lock()
            .unwrap()
            .get_socket::<smoltcp::socket::UdpSocket>(self.handle))
    }

    pub fn bind(&mut self, addr: smoltcp::wire::IpAddress, port: u16) {
        self.with(|s| {
            s.bind((addr, port)).expect("Unable to UDP bind");
        })
    }

    pub fn send(&mut self, payload: &[u8], endpoint: IpEndpoint) {
        self.with(|s| {
            let _ = s.send_slice(payload, endpoint);
        });
        let _ = self
            .iface
            .lock()
            .unwrap()
            .poll(smoltcp::time::Instant::now());
    }
}

impl Drop for UdpSocket {
    fn drop(&mut self) {
        self.iface.lock().unwrap().remove_socket(self.handle);
    }
}

impl Stream for UdpSocket {
    type Item = (Bytes, IpEndpoint);

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        self.with(|s| match s.can_recv() {
            true => match s.recv() {
                Err(_) => Poll::Ready(None),
                Ok((payload, endpoint)) => {
                    Poll::Ready(Some((Bytes::copy_from_slice(payload), endpoint.clone())))
                }
            },
            false => {
                s.register_recv_waker(cx.waker());
                Poll::Pending
            }
        })
    }
}

impl TcpSocket {
    pub fn new(iface: IFace, s: smoltcp::socket::TcpSocket<'static>) -> Self {
        let handle = iface.lock().unwrap().add_socket(s);
        Self { handle, iface }
    }

    fn with<R>(&mut self, f: impl FnOnce(&mut smoltcp::socket::TcpSocket) -> R) -> R {
        f(self
            .iface
            .lock()
            .unwrap()
            .get_socket::<smoltcp::socket::TcpSocket>(self.handle))
    }

    pub fn dest(&mut self) -> (smoltcp::wire::IpAddress, u16) {
        self.with(|s| {
            let endpoint = s.local_endpoint();
            (endpoint.addr, endpoint.port)
        })
    }
}

impl Drop for TcpSocket {
    fn drop(&mut self) {
        self.iface.lock().unwrap().remove_socket(self.handle);
    }
}

impl AsyncRead for TcpSocket {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        self.with(|s| match s.can_recv() {
            true => {
                let rbuf = s.recv_slice(buf.initialize_unfilled());
                match rbuf {
                    Err(e) => {
                        return Poll::Ready(Err(std::io::Error::new(
                            std::io::ErrorKind::Other,
                            format!("{}", e),
                        )))
                    }
                    Ok(n) => {
                        if n > 0 {
                            buf.set_filled(n);
                            Poll::Ready(Ok(()))
                        } else {
                            s.register_recv_waker(cx.waker());
                            Poll::Pending
                        }
                    }
                }
            }
            false => {
                s.register_recv_waker(cx.waker());
                Poll::Pending
            }
        })
    }
}

impl AsyncWrite for TcpSocket {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        let p = self.with(|s| match s.can_send() {
            true => match s.send_slice(buf) {
                Ok(0) => {
                    s.register_send_waker(cx.waker());
                    Poll::Pending
                }
                Ok(n) => Poll::Ready(Ok(n)),
                Err(e) => Poll::Ready(Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("{}", e),
                ))),
            },
            false => {
                s.register_send_waker(cx.waker());
                Poll::Pending
            }
        });
        match p {
            Poll::Ready(_) => {
                // We need to poll in order to process the ingress packets.
                let _ = self
                    .iface
                    .lock()
                    .unwrap()
                    .poll(smoltcp::time::Instant::now());
            }
            _ => (),
        };
        p
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        self.with(|s| s.close());
        Poll::Ready(Ok(()))
    }
}
