use onion_tunnel::OnionTunnel;
use simple_logger::SimpleLogger;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    SimpleLogger::new().with_utc_timestamps().init().unwrap();

    let mut onion_tunnel = OnionTunnel::new("onion0", Default::default()).await;

    tokio::select! {
        _ = onion_tunnel.start() => (),
    };

    Ok(())
}
