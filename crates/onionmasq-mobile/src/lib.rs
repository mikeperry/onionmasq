#![allow(non_snake_case)]

#[cfg(target_os = "android")]
use android_logger::Config;
#[cfg(target_os = "android")]
use jni::objects::{JClass, JString};
#[cfg(target_os = "android")]
use jni::JNIEnv;
use log::debug;
use log::Level;
use onion_tunnel::OnionTunnel;

#[cfg(target_os = "android")]
#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_runProxy(
    env: JNIEnv,
    _: JClass,
    fd: i32,
    interface_name: JString,
    cache_dir: JString,
) {
    debug!("Onionmasq_runProxy");
    let rt = tokio::runtime::Runtime::new().expect("failed to create tokio runtime");
    let cache_dir = env.get_string(cache_dir).expect("cache_dir is invalid");
    let cache_dir = cache_dir.to_string_lossy();
    // TODO arti data should probably be put in the datadir, not the cachedir
    let config = onion_tunnel::TorClientConfigBuilder::from_directories(
        format!("{}/arti-data", cache_dir),
        format!("{}/arti-cache", cache_dir),
    )
    .build()
    .unwrap();
    rt.block_on(async move {
        debug!("creating onion_tunnel...");

        let mut onion_tunnel: OnionTunnel;
        match OnionTunnel::create_with_fd(
            &env.get_string(interface_name)
                .expect("interface_name is invalid")
                .to_string_lossy(),
            fd,
            config,
        )
        .await
        {
            Ok(v) => {
                debug!("successfully created tun interface");
                onion_tunnel = v;
            }
            Err(e) => {
                debug!("couldn't start onionmasq proxy ({})", e);
                return;
            }
        }
        debug!("starting onionmasq...");
        _ = onion_tunnel.start().await;
        debug!("stopped onionmasq...");
    });
}

#[cfg(target_os = "android")]
#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_initLogging(_env: JNIEnv, _: JClass) {
    android_logger::init_once(
        Config::default()
            .with_min_level(Level::Trace)
            .with_tag("onionmasq"),
    );
    debug!("logger initialized");
}

/// Android 5.0 to 6.0 misses this function, which prevent Arti from running. This is a translation
/// to Rust of Musl implementation. If you don't plan to support anything below Android 7.0, you
/// should probably not copy this code.
/// It might be possible to support Android 4.4 and below with the same trick applied to more
/// functions (at least create_epoll1), but this might not be worth the effort.
#[cfg(target_os = "android")]
#[no_mangle]
pub unsafe extern "C" fn lockf(fd: libc::c_int, cmd: libc::c_int, len: libc::off_t) -> libc::c_int {
    use libc::*;
    let mut l = flock {
        l_type: F_WRLCK as i16,
        l_whence: SEEK_CUR as i16,
        l_len: len,
        l_pid: 0,
        l_start: 0,
    };
    match cmd {
        F_TEST => {
            l.l_type = F_RDLCK as i16;
            if fcntl(fd, F_GETLK, &l) < 0 {
                return -1;
            }
            if l.l_type == F_UNLCK as i16 || l.l_pid == getpid() {
                return 0;
            }
            *__errno() = EACCES;
            -1
        }
        F_ULOCK => {
            l.l_type = F_UNLCK as i16;
            fcntl(fd, F_SETLK, &l)
        }
        F_TLOCK => fcntl(fd, F_SETLK, &l),
        F_LOCK => fcntl(fd, F_SETLKW, &l),
        _ => {
            *__errno() = EINVAL;
            -1
        }
    }
}
