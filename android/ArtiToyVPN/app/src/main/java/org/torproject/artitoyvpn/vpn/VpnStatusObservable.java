package org.torproject.artitoyvpn.vpn;

import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STOPPED;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.torproject.artitoyvpn.ui.logging.LogObservable;
import org.torproject.artitoyvpn.utils.Utils;

public class VpnStatusObservable {
    public enum Status {
        STOPPED,
        STARTING,
        RUNNING,
        STOPPING,
        ERROR
    }

    private final MutableLiveData<VpnStatusObservable.Status> statusLiveData;

    private static VpnStatusObservable instance;

    private VpnStatusObservable() {
        statusLiveData = new MutableLiveData<>(STOPPED);
    }

    public static VpnStatusObservable getInstance() {
        if (instance == null) {
            instance = new VpnStatusObservable();
        }
        return instance;
    }

    public static void update(Status status) {
        if (Utils.isRunningOnMainThread()) {
            instance.statusLiveData.setValue(status);
        } else {
            instance.statusLiveData.postValue(status);
        }
        LogObservable.getInstance().addLog(status.toString());
    }

    public static LiveData<Status> getStatus() {
        return instance.statusLiveData;
    }

}
