package org.torproject.artitoyvpn.utils;


import static org.torproject.artitoyvpn.vpn.ArtiVpnService.ACTION_STOP_VPN;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import org.torproject.artitoyvpn.MainActivity;
import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.vpn.ArtiVpnService;


public class VpnNotificationManager {


    private final Context context;
    public final static int ARTI_NOTIFICATION_ID = 1533082945;
    private final static String NOTIFICATION_CHANNEL_NEWSTATUS_ID = "ARTI_NOTIFICATION_CHANNEL_NEWSTATUS_ID";

    public VpnNotificationManager(@NonNull Context context) {
        this.context = context;
    }

    public Notification buildForegroundServiceNotification() {
        NotificationManager notificationManager = initNotificationManager();
        if (notificationManager == null) {
            return null;
        }
        NotificationCompat.Action.Builder actionBuilder = new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_close_clear_cancel,
                "STOP", getStopIntent());
        NotificationCompat.Builder notificationBuilder = initNotificationBuilderDefaults();
        notificationBuilder
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(context.getString(R.string.stop))
                .setContentIntent(getContentPendingIntent())
                .addAction(actionBuilder.build());

        return notificationBuilder.build();
    }

    private PendingIntent getContentPendingIntent() {
        Intent mainActivityIntent = new Intent(context, MainActivity.class);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return PendingIntent.getActivity(context, 0, mainActivityIntent, getDefaultFlags());
    }
    private PendingIntent getStopIntent() {
        Intent stopVpnIntent = new Intent (context, ArtiVpnService.class);
        stopVpnIntent.setAction(ACTION_STOP_VPN);

        return PendingIntent.getService(context, 0, stopVpnIntent, getDefaultFlags());
    }

    private int getDefaultFlags() {
        int flag = PendingIntent.FLAG_CANCEL_CURRENT;
        if (Build.VERSION.SDK_INT >= 23) {
            flag |= PendingIntent.FLAG_IMMUTABLE;
        }
        return flag;
    }

    private NotificationManager initNotificationManager() {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(notificationManager);
        }
        return notificationManager;
    }

    @TargetApi(26)
    private void createNotificationChannel(NotificationManager notificationManager) {
        CharSequence name = context.getString(R.string.notification_channel_name);
        String description = context.getString(R.string.notification_channel_description);
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_NEWSTATUS_ID,
                name,
                NotificationManager.IMPORTANCE_LOW);
        channel.setSound(null, null);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        notificationManager.createNotificationChannel(channel);
    }

    private NotificationCompat.Builder initNotificationBuilderDefaults() {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.context, NOTIFICATION_CHANNEL_NEWSTATUS_ID);
        notificationBuilder.
                setDefaults(Notification.DEFAULT_ALL).
                setAutoCancel(true);
        return notificationBuilder;
    }



    public void cancelNotifications() {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            return;
        }
        notificationManager.cancel(ARTI_NOTIFICATION_ID);
    }
}
