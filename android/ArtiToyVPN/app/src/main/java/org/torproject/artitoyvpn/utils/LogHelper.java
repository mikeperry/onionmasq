package org.torproject.artitoyvpn.utils;

import android.util.Log;

import org.torproject.artitoyvpn.ui.logging.LogObservable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;

public class LogHelper {
    AtomicBoolean markInvalid = new AtomicBoolean(false);

    public void stopLog() {
        markInvalid.set(true);
    }


    public void readLog() {
        Thread t = new Thread(() -> {
                LogObservable.getInstance().addLog("Start reading onionmasq logs from logcat");
                String cmd = "logcat -v tag onionmasq:V StreamCapture:D *:S";
                try {
                    Runtime.getRuntime().exec("logcat -c");
                    Process process = Runtime.getRuntime().exec(cmd);
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(process.getInputStream()));

                    String line = "";
                    while ((line = bufferedReader.readLine()) != null && !markInvalid.get()) {
                        LogObservable.getInstance().addLog(line);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                LogObservable.getInstance().addLog("Stop reading onionmasq logs from logcat");
        });
        t.start();
    }



    /**
     * Add a new tag to file log.
     *
     * @param tag      The android {@link Log} tag, which should be logged into the file.
     * @param priority The priority which should be logged into the file. Can be V, D, I, W, E, F
     *
     * @see <a href="http://developer.android.com/tools/debugging/debugging-log.html#filteringOutput">Filtering Log Output</a>
     */
   /* public void addLogTag(String tag, String priority){
        String newEntry = " " + tag + ":" + priority;
        if(!cmdEnd.contains(newEntry)){
            cmdEnd = newEntry + cmdEnd;
            if(isLogStarted){
                startLog();
            }else{
                initLog();
            }
        }
    }*/
}
