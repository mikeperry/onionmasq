package org.torproject.artitoyvpn.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

public class PreferenceHelper {
    private final String preferenceName = "ArtyToyVPNPRref";
    private final String selectedAppSetName = "selectedAppSet";
    private final String showLogTimestamps = "showLogTimestamps";
    SharedPreferences sharedPreferences;

    public PreferenceHelper(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
    }

    /**
     *
     * @return A Set of strings if selection is done(all selected, some selected or none selected), null otherwise().
     */
    public Set<String> getSelectedApps() {
        return sharedPreferences.getStringSet(selectedAppSetName, null);
    }

    //apply is important, otherwise this may freeze the main thread for few millis.
    public void setSelectedApps(Set<String> selectedApps) {
        sharedPreferences.edit().putStringSet(selectedAppSetName, selectedApps).apply();
    }

    public void showLogTimestamps(Boolean show) {
        sharedPreferences.edit().putBoolean(showLogTimestamps, show).apply();
    }

    public Boolean getShowLogTimestamps() {
        return sharedPreferences.getBoolean(showLogTimestamps, false);
    }
}
