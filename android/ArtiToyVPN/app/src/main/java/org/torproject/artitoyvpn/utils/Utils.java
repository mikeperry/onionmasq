package org.torproject.artitoyvpn.utils;

import android.content.ClipData;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import org.torproject.artitoyvpn.BuildConfig;
import org.torproject.artitoyvpn.R;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class Utils {
    private static final String TAG = "ARTY_VPN";

    public static boolean isRunningOnMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }


    public static void runWithDelay(long delay, Runnable runnable) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runnable.run();
            }
        }, delay);
    }

    public static boolean isMainThread() {
        return Looper.myLooper() != null && Looper.myLooper() == Looper.getMainLooper();
    }

    public static void runOnMain(final @NonNull Runnable runnable) {
        if (isMainThread()) runnable.run();
        else {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    public static void writeTextToClipboard(@NonNull Context context, @NonNull String text) {
        android.content.ClipboardManager clipboard =
                (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(context.getString(R.string.app_name), text);
        clipboard.setPrimaryClip(clip);
    }

    public static String getFormattedDate(long timestamp, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss:SSS", locale);
        return sdf.format(timestamp);
    }

    public static void logD(String message) {
        logD(TAG, message);
    }

    public static void logD(String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

}
